// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// This allows us to use all the routes defined in "userRoute.js"
const userRoute = require("./routes/userRoute");
// This allows us to use all the routes defined in "courseRoute.js"
const courseRoute = require("./routes/courseRoute");

// Server
const app = express();

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
// localhost:3000/users
app.use("/users", userRoute);
// Allows all the user routes created in "courseRoute.js" file to use "/courses" as route (resources) 
app.use("/courses", courseRoute);

// Database Connection 
mongoose.connect("mongodb+srv://nathanieljaen12:admin123@zuitt-bootcamp.vs10cer.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log('Now connected to the cloud database'));


// Server listening
// Will use the defined port number for the application whenever environment variable is available or used port 3000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 3000, () => console.log(`Now connected to port ${process.env.PORT || 3000}`) )
 
