const express = require("express");
// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for checking is tha user's email already exists in the database

router.post("/checkEmail", (req, res) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.post("/details", auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

// Route for authenticated user enrollment
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})




module.exports = router;

