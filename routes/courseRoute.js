const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth");

// Route for creating a course
// router.post("/create", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// });

router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Retrieve ACTIVE courses
router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating specific course
router.put("/:courseId", (req, res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route for archiving specific rouse
router.patch("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;

