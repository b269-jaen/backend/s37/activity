const Course = require("../models/Course");
const auth = require("../auth")


module.exports.addCourse = (data) => {
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then((course, err) =>{
			if(err){
				return false;
			} else {
				return true;
			}
		});
	} 

	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});

};

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result =>{
		return result;
	})
};

module.exports.updateCourse = (reqParams, reqBody) =>{
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, err) =>{
		if(err){
			return false;
		} else {
			return true;
		}
	})
};

// s40 Activity
// Archiving a course
module.exports.archiveCourse = (reqParams) => {
	let archived = {
		isActive: false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archived).then((archive, err) => {
		if(err){
			return false;
		}else{
			return true;
		}
	})
};

